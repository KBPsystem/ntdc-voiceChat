
//Initialize dependencies
const express = require('express');
const app = express();

const port = 3000;

const APIAI_TOKEN = 'e1abf759491f4b7eb4b2ed61c2a32f5e';
const APIAI_SESSION_ID = '1aab5094d5fd4fde8d90ecb4b6bb80ef';

//Declare static pages.
app.use(express.static(__dirname + '/views')); 
app.use(express.static(__dirname + '/public'));

//Express Infrastructure 
const server = app.listen(port);
console.log(`Server running on http://localhost:${port}`);


const io = require('socket.io')(server);
io.on('connection', function(socket){
    console.log('User Connected');
});

const apiai = require('apiai')(APIAI_TOKEN);


//Serve frontend files after turning local server ON
app.get('/', (req, res) => {
    res.sendFile('index.html');
});


io.on('connection', function(socket) {
    socket.on('chat message', (text) => {
        console.log('Message' + text);
        //Reply from AI
        let apiaiReq = apiai.textRequest(text, {
            sessionId: APIAI_SESSION_ID
        });

        apiaiReq.on('response', (response) => {
            let aiText = response.result.fulfillment.speech;
            console.log('Bot Reply: ' + aiText);
            socket.emit('bot reply', aiText) //Sending Results to browser
        });

        apiaiReq.on('error', (error) => {
            console.log(error);
        });

        apiaiReq.end();
    });
});







