# NxtTrip Voice Chat
Voice chat using app for NxtTrip Services

### Official Website
[https://ntdc-webchat.now.sh](https://ntdc-webchat.now.sh)


App Screenshot:

![App Img](https://github.com/KBPsystem777/VoiceChat/blob/master/public/img/voicechat.png?raw=true)


### Development

```
> git clone https://gitlab.com/KBPsystem/ntdc-voiceChat
> cd voiceChat
> npm install
> npm run start
> nodemon index.js
> [nodemon] 1.17.5
  [nodemon] to restart at any timen enter `rs`
  [nodemon] watching: *.*
  [nodemon] starting `node index.js`
  Server running on http://localhost:3000
```

